package filereceiver

import (
	"fmt"
	"testing"
)

/*对于FileReceiver代码测试读取不正确的实例*/
func testFileReceiverFail(t *testing.T) {
	t.Helper()

	expectFileContent := FileReceiver("FileReceiver.go")
	actualFileContent := FileReceiver("testFileReceiver_fail.txt")

	if actualFileContent != expectFileContent {
		fmt.Println("test fail！")
		t.Errorf("content of Two compared file is different: expect「 %s 」\n------------------------------------------------------------\n actual「%s", expectFileContent ,actualFileContent)
	} else {
		fmt.Println("test succeed。")
	}
}