package filereceiver

import (
	"log"
	"os"
)

/*读取指定文件的内容*/
func FileReceiver(filePath string) string {
	fi, err := os.Open(filePath)
	if err != nil {
		log.Fatal()
	}
	defer fi.Close()
	fileInfo, err := fi.Stat()
	if err != nil {
		log.Fatal(err)
	}

	b := make([]byte,fileInfo.Size())
	_, err = fi.Read(b)
	if err != nil {
		log.Fatal(err)
	}
	
	return string(b) 
}