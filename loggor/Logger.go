package loggor

import (
	"log"
	"os"
)

func Logger(errContent string) {
	   file, err := os.OpenFile("error.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	   if err != nil {
		   log.Fatal(err)
	   }
   
	   // 创建一个新的日志记录器
	   logger := log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
   
	   // 写入一条错误信息
	   logger.Println("这是一条错误信息")
}